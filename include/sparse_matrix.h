#ifndef _SPARSE_MATRIX_HPP_CC_INCLUDED_DSOFIJ43T89USFDIJASFIUHFSDIUHGFIU4398YDFGKUHVKJHSDFIU9487YDFI
#define _SPARSE_MATRIX_HPP_CC_INCLUDED_DSOFIJ43T89USFDIJASFIUHFSDIUHGFIU4398YDFGKUHVKJHSDFIU9487YDFI

#include <stdint.h> //for uint32_t

struct sparse_matrix;

typedef double value_type;//precision control of the sparse matrix, can be float, double or long double

/*
 * Comment:
 *          return the rows of the given matrix
 * Input:
 *          A   :   the pointer to the sparse matrix
 * Return:
 *          the rows of the matrix
 */
uint32_t sparse_matrix_row( struct sparse_matrix* A );

/*
 * Comment:
 *          return the columns of the given matrix
 * Input:
 *          A   :   the pointer to the sparse matrix
 * Return:
 *          the columns of the matrix
 */
uint32_t sparse_matrix_col( struct sparse_matrix* A );

/*
 * Comment:
 *          create an instance of a sparse matrix whose size is r by c
 * Input:
 *          r   :   rows of the sparse matrix to be generated
 *          c   :   columns of the sparse matrix to be generated
 * Return:
 *          the pointer to the sparse matrix generated
 */
struct sparse_matrix  *sparse_matrix_generate( uint32_t r, uint32_t c );

/*
 * Comment:
 *          destroy the instance of a sparse matrix
 * Input:
 *          A   :   the pointer to the sparse matrix to be destroyed
 * Return:
 *          void
 */
void sparse_matrix_destroy( struct sparse_matrix *A );

/*
 * Comment:
 *          calculate the vector b for the matrix-vector multiplication
 *                  b = A x
 * Input:
 *          A   :   the pointer to the sparse matrix
 *          x   :   the pointer to the the first position of vector x
 *          b   :   the pointer to the the first position of vector b
 * Output:
 *          the pointer to the first position of the vector b
 */
value_type *sparse_matrix_multiply( struct sparse_matrix *A, value_type *x, value_type *b );

/*
 * Comment:
 *          calculate the vector b for the vector-matrix multiplication
 *                  b = x A
 * Input:
 *          A   :   the pointer to the sparse matrix
 *          x   :   the pointer to the the first position of vector x
 *          b   :   the pointer to the the first position of vector b
 * Output:
 *          the pointer to the first position of the vector b
 */
value_type *sparse_matrix_left_multiply( value_type *x, struct sparse_matrix *A, value_type *b );

/*
 * Comment:
 *          insert value v to sparse matrix A, in row r, column c
 * Input:
 *          A   :   the pointer to the sparse matrix
 *          r   :   the row index
 *          c   :   the column index
 *          v   :   the value to be inserted
 * Return:
 *          void
 */
void sparse_matrix_insert( struct sparse_matrix *A, uint32_t r, uint32_t c, value_type v );

/*
 * Comment:
 *          get the element value of matrix A, in row r, column c
 *  Input:
 *          A   :   the pointer to the sparse matrix
 *          r   :   the row index
 *          c   :   the column index
 *  Return:
 *          the value of A[r][c]
 */
value_type sparse_matrix_at( struct sparse_matrix *A, uint32_t r, uint32_t c );

#endif//_SPARSE_MATRIX_HPP_CC_INCLUDED_DSOFIJ43T89USFDIJASFIUHFSDIUHGFIU4398YDFGKUHVKJHSDFIU9487YDFI

