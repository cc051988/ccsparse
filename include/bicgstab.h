#ifndef _BICGSTAB_H_INCLUDED_CC_SFDOI498USDFKJHAGO9HIU3498YASFDKGHJVDJKDFKJH9UH4TUIHDFSG9HU7RT9HFDGS
#define _BICGSTAB_H_INCLUDED_CC_SFDOI498USDFKJHAGO9HIU3498YASFDKGHJVDJKDFKJH9UH4TUIHDFSG9HU7RT9HFDGS

#include <sparse_matrix.h> //for struct sparse_matrix and value_type

#include <stddef.h>

/*
 * Comment:
 *              solve equation Ax = b using biconjugate gradient stablized method
 * Input:
 *              A   :   the pointer to the sparse matrix A
 *              x   :   the pointer to the first position of vector x
 *              b   :   the pointer to the first position of vector b
 *            loops :   max iteration
 *  Output:
 *              the pointer to the first position of vector x
 */
value_type* bicgstab( struct sparse_matrix* A, value_type* x, value_type* b );

#endif//_BICGSTAB_H_INCLUDED_CC_SFDOI498USDFKJHAGO9HIU3498YASFDKGHJVDJKDFKJH9UH4TUIHDFSG9HU7RT9HFDGS

