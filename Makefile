#CC = gcc
OPTIONS = -O2 -Wall -I./include
BICGSTABSOURCES = \
src/bicgstab.c \
src/sparse_matrix.c

OBJECTS=$(BICGSTABSOURCES:.c=.o)	
STATIC_LIB=./lib/libbicgstab.a

all: $(BICGSTABSOURCES) $(STATIC_LIB)

$(STATIC_LIB): $(OBJECTS)
	ar rc $@ $(OBJECTS)			 

.c.o:
	$(CC) -c $(OPTIONS) $< -o $@  
	
clean:
	rm -f src/*.o

distclean: clean
	rm -f $(STATIC_LIB)

