#include <sparse_matrix.h> 

#include <stdint.h>
#include <stdlib.h>
#include <stddef.h>
#include <assert.h>
#include <math.h>

static value_type inner_product( value_type* vec1_first, value_type* vec1_last, value_type* vec2_first )
{
    value_type ans = 0;
    while( vec1_first != vec1_last )
        ans += (*vec1_first++) * (*vec2_first++);
    return ans;
}

static value_type* array_array_ops( value_type* vec1_first, value_type* vec1_last, value_type factor1,
                                    value_type* vec2_first, value_type factor2,
                                    value_type* vec3_first )
{
    while ( vec1_first != vec1_last )
        (*vec3_first++) = factor1 * (*vec1_first++) + factor2 * (*vec2_first++);
    return vec3_first;
}

static value_type* array_array_array_ops( value_type* vec1_first, value_type* vec1_last, value_type factor1,
                                          value_type* vec2_first, value_type factor2,
                                          value_type* vec3_first, value_type factor3,
                                          value_type* vec4_first )
{
    while ( vec1_first != vec1_last )
        (*vec4_first++) = factor1 * (*vec1_first++) + factor2 * (*vec2_first++) + factor3 * (*vec3_first++);
    return vec4_first;
}

static value_type* array_copy( value_type* vec1_first, value_type* vec1_last, value_type* vec2_first )
{
    while ( vec1_first != vec1_last )
        (*vec1_first++) = (*vec2_first++);
    return vec2_first;
}

static value_type* array_fill( value_type* vec1_first, value_type* vec1_last, value_type v )
{
    while ( vec1_first != vec1_last )
        (*vec1_first++) = v;
    return vec1_first;
}

static value_type included_angle( value_type* vec1_first, value_type* vec1_last, value_type* vec2_first )
{
    value_type x1x2 = inner_product( vec1_first, vec1_last, vec2_first );
    value_type x1   = sqrt( inner_product( vec1_first, vec1_last, vec1_first ) );
    value_type x2   = sqrt( inner_product( vec2_first, vec1_last-vec1_first+vec2_first, vec2_first ) );
    return acos( x1x2/(x1*x2) );
}

/*
 * Comment:
 *              solve equation Ax = b using biconjugate gradient stablized method
 * Input:
 *              A   :   the pointer to the sparse matrix A
 *              x   :   the pointer to the first position of vector x
 *              b   :   the pointer to the first position of vector b
 *  Output:
 *              the pointer to the first position of vector x
 */
value_type* bicgstab( struct sparse_matrix* A, value_type* x, value_type* b )
{
    assert( sparse_matrix_row(A) == sparse_matrix_col(A) );
    size_t n = sparse_matrix_row(A);
    value_type * r0= (value_type*) malloc( sizeof(value_type) * n ); 
    value_type * r = (value_type*) malloc( sizeof(value_type) * n ); 
    value_type * r_= (value_type*) malloc( sizeof(value_type) * n ); 
    value_type * v = (value_type*) malloc( sizeof(value_type) * n ); 
    value_type * p = (value_type*) malloc( sizeof(value_type) * n ); 
    value_type * s = (value_type*) malloc( sizeof(value_type) * n ); 
    value_type * t = (value_type*) malloc( sizeof(value_type) * n ); 
    value_type * _ = (value_type*) malloc( sizeof(value_type) * n ); 
    value_type rho = 1;
    value_type rho_ = 1;
    value_type alpha = 1;
    value_type beta = 1;
    value_type omega = 1;
    value_type eps = 1.0e-5;

    sparse_matrix_multiply( A, x, _ ); // _ = A*x
    array_array_ops( b, b+n, 1, _, -1, r ); // r = b - _
    array_copy( r, r+n, r0 ); // r0 = r
    array_copy( r, r+n, r_ ); // r_ = r
    array_fill( v, v+n, 0 ); // v = 0
    array_fill( p, p+n, 0 ); // p = 0

    for (;;)
    {
        rho_ = inner_product( r0, r0+n, r ); // rho_i = ( r0, r_{i-1} )
        beta = rho_ * alpha / ( rho * omega ); // beta = (rho_i / rho_{i-1}) / (alpha/omega_{i-1}) 
        rho = rho_;
        array_array_array_ops( r, r+n, 1, p, beta, v, -beta*omega, p ); // p = r + \beta p - \beta \omega v
        sparse_matrix_multiply( A, p, v ); // v = A p
        alpha = rho / inner_product( r0, r0+n, v ); // alpha = rho_i / ( r_0, v )
        array_array_ops( r, r+n, 1, v, alpha, s ); // s = r - alpha v
        sparse_matrix_multiply( A, s, t );
        omega = inner_product( t, t+n, s ) / inner_product( t, t+n, t ); // omega = (t,s)/(t,t)
        array_array_array_ops( x, x+n, 1, p, alpha, s, omega, x ); // x = x + alpha p + omega s
        array_array_ops( s, s+n, 1, t, -omega, r ); // r = s - \omega t
        sparse_matrix_multiply( A, x, _ ); // _ = A*x
        if ( included_angle( _, _+n, b ) <= eps ) break;
    }

    free (r0);
    free (r);
    free (r_);
    free (v);
    free (p);
    free (s);
    free (t);
    free (_);
    return x; 
}

