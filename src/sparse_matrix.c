#include <sparse_matrix.h>

#include <stdint.h>
#include <stdlib.h>
#include <stddef.h>

enum rb_color
{
    RB_BLACK,
    RB_RED,
};

struct rbtree_node
{
    struct rbtree_node *left, *right, *parent;
    enum rb_color color;
};

typedef int32_t ( *rbtree_cmp_fn_t )( struct rbtree_node *, struct rbtree_node * );

struct rbtree
{
    struct rbtree_node *root;
    rbtree_cmp_fn_t cmp_fn;
    struct rbtree_node *first, *last;
    uint64_t reserved[4];
};

typedef struct rbtree_node node_type;
typedef struct rbtree tree_type;
typedef enum rb_color color_type;

#ifndef offsetof
#define offsetof(s,m) (size_t)&(((s *)0)->m)
#endif//offsetof
#define rbtree_container_of(node, type, member) ((type *)((char *)(node) - offsetof(type, member)))

static color_type __get_color( node_type *node )
{
    return node->color;
}

static void __set_color( color_type color, node_type *node )
{
    node->color = color;
}

static node_type *__get_parent( node_type *node )
{
    return node->parent;
}

static void __set_parent( node_type *parent, node_type *node )
{
    node->parent = parent;
}

static int __is_root( node_type *node )
{
    return 0 == __get_parent( node );
}

static int __is_black( node_type *node )
{
    return __get_color( node ) == RB_BLACK;
}

static int __is_red( node_type *node )
{
    return !__is_black( node );
}

static node_type *__get_first( node_type *node )
{
    while ( node->left )
        node = node->left;
    return node;
}

static node_type *__get_last( node_type *node )
{
    while ( node->right )
        node = node->right;
    return node;
}

static node_type *rbtree_first( tree_type *tree )
{
    return tree->first;
}

static node_type *rbtree_last( tree_type *tree )
{
    return tree->last;
}

static node_type *rbtree_next( node_type *node )
{
    node_type *parent;
    if ( node->right )
        return __get_first( node->right );
    while ( ( parent = __get_parent( node ) ) && parent->right == node )
        node = parent;
    return parent;
}

static node_type *rbtree_prev( node_type *node )
{
    node_type *parent;
    if ( node->left )
        return __get_last( node->left );
    while ( ( parent = __get_parent( node ) ) && parent->left == node )
        node = parent;
    return parent;
}

static node_type *__lookup( node_type *key, tree_type *tree, node_type **pparent, int *is_left )
{
    node_type *node = tree->root;
    *pparent = 0;
    *is_left = 0;
    while ( node )
    {
        int32_t res = tree->cmp_fn( node, key );
        if ( res == 0 )
            return node;
        *pparent = node;
        if ( ( *is_left = res > 0 ) )
            node = node->left;
        else
            node = node->right;
    }
    return 0;
}

static void __left_rotate( node_type *node, tree_type *tree )
{
    node_type *p = node;
    node_type *q = node->right; /* can't be 0 */
    node_type *parent = __get_parent( p );
    if ( !__is_root( p ) )
    {
        if ( parent->left == p )
            parent->left = q;
        else
            parent->right = q;
    }
    else
        tree->root = q;
    __set_parent( parent, q );
    __set_parent( q, p );
    p->right = q->left;
    if ( p->right )
        __set_parent( p, p->right );
    q->left = p;
}

static void __right_rotate( node_type *node, tree_type *tree )
{
    node_type *p = node;
    node_type *q = node->left; /* can't be 0 */
    node_type *parent = __get_parent( p );
    if ( !__is_root( p ) )
    {
        if ( parent->left == p )
            parent->left = q;
        else
            parent->right = q;
    }
    else
        tree->root = q;
    __set_parent( parent, q );
    __set_parent( q, p );
    p->left = q->right;
    if ( p->left )
        __set_parent( p, p->left );
    q->right = p;
}

static node_type *rbtree_lookup( node_type *key, tree_type *tree )
{
    node_type *parent;
    int is_left;
    return __lookup( key, tree, &parent, &is_left );
}

static void __set_child( node_type *child, node_type *node, int left )
{
    if ( left )
        node->left = child;
    else
        node->right = child;
}

static node_type *rbtree_insert( node_type *node, tree_type *tree )
{
    node_type *key, *parent;
    int is_left;
    key = __lookup( node, tree, &parent, &is_left );
    if ( key )
        return key;
    node->left = 0;
    node->right = 0;
    __set_color( RB_RED, node );
    __set_parent( parent, node );
    if ( parent )
    {
        if ( is_left )
        {
            if ( parent == tree->first )
                tree->first = node;
        }
        else
        {
            if ( parent == tree->last )
                tree->last = node;
        }
        __set_child( node, parent, is_left );
    }
    else
    {
        tree->root = node;
        tree->first = node;
        tree->last = node;
    }
    while ( ( parent = __get_parent( node ) ) && __is_red( parent ) )
    {
        node_type *grandpa = __get_parent( parent );
        if ( parent == grandpa->left )
        {
            node_type *uncle = grandpa->right;
            if ( uncle && __is_red( uncle ) )
            {
                __set_color( RB_BLACK, parent );
                __set_color( RB_BLACK, uncle );
                __set_color( RB_RED, grandpa );
                node = grandpa;
            }
            else
            {
                if ( node == parent->right )
                {
                    __left_rotate( parent, tree );
                    node = parent;
                    parent = __get_parent( node );
                }
                __set_color( RB_BLACK, parent );
                __set_color( RB_RED, grandpa );
                __right_rotate( grandpa, tree );
            }
        }
        else
        {
            node_type *uncle = grandpa->left;
            if ( uncle && __is_red( uncle ) )
            {
                __set_color( RB_BLACK, parent );
                __set_color( RB_BLACK, uncle );
                __set_color( RB_RED, grandpa );
                node = grandpa;
            }
            else
            {
                if ( node == parent->left )
                {
                    __right_rotate( parent, tree );
                    node = parent;
                    parent = __get_parent( node );
                }
                __set_color( RB_BLACK, parent );
                __set_color( RB_RED, grandpa );
                __left_rotate( grandpa, tree );
            }
        }
    }
    __set_color( RB_BLACK, tree->root );
    return 0;
}

static void rbtree_remove( node_type *node, tree_type *tree )
{
    node_type *parent = __get_parent( node );
    node_type *left = node->left;
    node_type *right = node->right;
    node_type *next;
    color_type color;
    if ( node == tree->first )
        tree->first = rbtree_next( node );
    if ( node == tree->last )
        tree->last = rbtree_prev( node );
    if ( !left )
        next = right;
    else
        if ( !right )
            next = left;
        else
            next = __get_first( right );
    if ( parent )
        __set_child( next, parent, parent->left == node );
    else
        tree->root = next;
    if ( left && right )
    {
        color = __get_color( next );
        __set_color( __get_color( node ), next );
        next->left = left;
        __set_parent( next, left );
        if ( next != right )
        {
            parent = __get_parent( next );
            __set_parent( __get_parent( node ), next );
            node = next->right;
            parent->left = node;
            next->right = right;
            __set_parent( next, right );
        }
        else
        {
            __set_parent( parent, next );
            parent = next;
            node = next->right;
        }
    }
    else
    {
        color = __get_color( node );
        node = next;
    }
    if ( node )
        __set_parent( parent, node );
    if ( color == RB_RED )
        return;
    if ( node && __is_red( node ) )
    {
        __set_color( RB_BLACK, node );
        return;
    }
    do
    {
        if ( node == tree->root )
            break;
        if ( node == parent->left )
        {
            node_type *sibling = parent->right;
            if ( __is_red( sibling ) )
            {
                __set_color( RB_BLACK, sibling );
                __set_color( RB_RED, parent );
                __left_rotate( parent, tree );
                sibling = parent->right;
            }
            if ( ( !sibling->left  || __is_black( sibling->left ) ) &&
                    ( !sibling->right || __is_black( sibling->right ) ) )
            {
                __set_color( RB_RED, sibling );
                node = parent;
                parent = __get_parent( parent );
                continue;
            }
            if ( !sibling->right || __is_black( sibling->right ) )
            {
                __set_color( RB_BLACK, sibling->left );
                __set_color( RB_RED, sibling );
                __right_rotate( sibling, tree );
                sibling = parent->right;
            }
            __set_color( __get_color( parent ), sibling );
            __set_color( RB_BLACK, parent );
            __set_color( RB_BLACK, sibling->right );
            __left_rotate( parent, tree );
            node = tree->root;
            break;
        }
        else
        {
            node_type *sibling = parent->left;
            if ( __is_red( sibling ) )
            {
                __set_color( RB_BLACK, sibling );
                __set_color( RB_RED, parent );
                __right_rotate( parent, tree );
                sibling = parent->left;
            }
            if ( ( !sibling->left  || __is_black( sibling->left ) ) &&
                    ( !sibling->right || __is_black( sibling->right ) ) )
            {
                __set_color( RB_RED, sibling );
                node = parent;
                parent = __get_parent( parent );
                continue;
            }
            if ( !sibling->left || __is_black( sibling->left ) )
            {
                __set_color( RB_BLACK, sibling->right );
                __set_color( RB_RED, sibling );
                __left_rotate( sibling, tree );
                sibling = parent->left;
            }
            __set_color( __get_color( parent ), sibling );
            __set_color( RB_BLACK, parent );
            __set_color( RB_BLACK, sibling->left );
            __right_rotate( parent, tree );
            node = tree->root;
            break;
        }
    }
    while ( __is_black( node ) );
    if ( node )
        __set_color( RB_BLACK, node );
}

static void rbtree_init( tree_type *tree, rbtree_cmp_fn_t fn )
{
    tree->root = 0;
    tree->cmp_fn = fn;
    tree->first = 0;
    tree->last = 0;
}

typedef struct
{
    uint32_t        row;
    uint32_t        col;
} row_col_type;

typedef union
{
    row_col_type    rc;
    uint64_t        key;
} key_type;

typedef struct
{
    key_type        the_key;
    value_type      the_value;
} key_value_associate_type;

typedef struct
{
    key_value_associate_type    key_value;
    node_type                   the_node;
} element_type;

int32_t element_compare_function( node_type *lhs, node_type *rhs )
{
    element_type *l = rbtree_container_of( lhs, element_type, the_node );
    element_type *r = rbtree_container_of( rhs, element_type, the_node );
    return ( int32_t )( ( *l ).key_value.the_key.key ) - ( int32_t )( ( *r ).key_value.the_key.key );
}

static uint32_t extract_row( node_type *node )
{
    element_type *elm = rbtree_container_of( node, element_type, the_node );
    return ( *elm ).key_value.the_key.rc.row;
}

static uint32_t extract_col( node_type *node )
{
    element_type *elm = rbtree_container_of( node, element_type, the_node );
    return ( *elm ).key_value.the_key.rc.col;
}

static uint32_t extract_val( node_type *node )
{
    element_type *elm = rbtree_container_of( node, element_type, the_node );
    return ( *elm ).key_value.the_value;
}

//remove an element from the tree
static void element_release( tree_type *t, node_type *node )
{
    rbtree_remove( node, t );
    element_type *elem = rbtree_container_of( node, element_type, the_node );
    free( elem );
    node = 0;
}

//insert an element to the tree
static node_type *element_insert( tree_type *t, uint32_t r, uint32_t c, value_type v )
{
    element_type *elem;
    node_type *node = 0;
    //create an instance of the element
    element_type el;
    el.key_value.the_key.rc.row = r;
    el.key_value.the_key.rc.col = c;
    //check if the key-value exists in the tree
    if ( ( node = rbtree_lookup( &( el.the_node ), t ) ) ) //if exists
    {
        if ( 0 == v ) //if the value is 0, simplely erase it
        {
            element_release( t, node );
            return 0;
        }
        //otherwise change the value of it
        elem = rbtree_container_of( node, element_type, the_node );
        ( *elem ).key_value.the_value = v;
        return node;
    }
    //the node does not exist, create an element for a new node
    elem = ( element_type * ) malloc( sizeof( element_type ) );
    //initialize the element
    ( *elem ).key_value.the_key.rc.row = r;
    ( *elem ).key_value.the_key.rc.col = c;
    ( *elem ).key_value.the_value      = v;
    //assign the node in the element to the tree
    return rbtree_insert( &( ( *elem ).the_node ), t );
}

//get an element from the tree
static value_type element_at( tree_type *t, uint32_t r, uint32_t c )
{
    node_type *node;
    element_type elem;
    elem.key_value.the_key.rc.row = r;
    elem.key_value.the_key.rc.col = c;
    if ( ( node = rbtree_lookup( &( elem.the_node ), t ) ) )
        return extract_val( node );
    return 0;
}

struct sparse_matrix
{
    uint32_t    row;
    uint32_t    col;
    tree_type  *tree;
};

typedef struct sparse_matrix sparse_matrix_type;

sparse_matrix_type  *sparse_matrix_generate( uint32_t r, uint32_t c )
{
    sparse_matrix_type *sp = ( sparse_matrix_type * ) malloc( sizeof( sparse_matrix_type ) );
    ( *sp ).row = r;
    ( *sp ).col = c;
    rbtree_init( ( *sp ).tree, element_compare_function );
    return sp;
}

void sparse_matrix_destroy( sparse_matrix_type *A )
{
    node_type *first = rbtree_first( ( *A ).tree );
    node_type *second;
    node_type *last = rbtree_last( ( *A ).tree );
    while ( first != last )
    {
        second = rbtree_next( first );
        element_release( ( *A ).tree, first );
        first = second;
    }
    free( A );
    A = 0;
}

value_type *sparse_matrix_multiply( sparse_matrix_type *A, value_type *x, value_type *b )
{
    node_type *node;
    uint32_t r;
    uint32_t c;
    uint32_t i;
    value_type v;
    for ( i = 0; i != ( *A ).row; ++i )
        *( b + i ) = 0;
    for ( node = rbtree_first( ( *A ).tree );
            node != rbtree_last( ( *A ).tree );
            node = rbtree_next( node ) )
    {
        r = extract_row( node );
        c = extract_col( node );
        v = extract_val( node );
        *( b + r ) += v * ( *( x + c ) );
    }
    return b;
}

value_type *sparse_matrix_left_multiply( value_type *x, sparse_matrix_type *A, value_type *b )
{
    node_type *node;
    uint32_t r;
    uint32_t c;
    uint32_t i;
    value_type v;
    for ( i = 0; i != ( *A ).col; ++i )
        *( b + i ) = 0;
    for ( node = rbtree_first( ( *A ).tree ); node != rbtree_last( ( *A ).tree ); node = rbtree_next( node ) )
    {
        r = extract_row( node );
        c = extract_col( node );
        v = extract_val( node );
        *( b + c ) += v * ( *( x + r ) );
    }
    return b;
}

void sparse_matrix_insert( sparse_matrix_type *A, uint32_t r, uint32_t c, value_type v )
{
    element_insert( ( *A ).tree, r, c, v );
}

value_type sparse_matrix_at( sparse_matrix_type *A, uint32_t r, uint32_t c )
{
    return element_at( ( *A ).tree, r, c );
}

uint32_t sparse_matrix_row( struct sparse_matrix* A )
{
    return (*A).row;
}

uint32_t sparse_matrix_col( struct sparse_matrix* A )
{
    return (*A).col;
}

